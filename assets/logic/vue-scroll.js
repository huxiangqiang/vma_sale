var code = getQueryString("code");
var sessionid = getQueryString("sessionid");
var enterpriseid = getQueryString("enterpriseid");
var page = {
	"page_num": 1,
	"page_size": 8
}
$(function () {
	if (sessionid == null) {
		aiUserInfo();
	} else {
		pageData(sessionid)
	}

})


/**
 * 获取用户信息及权限
 */
function aiUserInfo() {
	$.ajax({
		type: 'POST',
		url: baseUrl + 'v3.0/login/ai?code=' + code+'&enterpriseid='+enterpriseid,
		contentType: 'application/json',
		success: function (data) {
			//无权限 则跳转
			if (data == 0) {
				window.location.href = "企业微信/没有权限.html";
			}
			//有权限
			else {
                sessionid=data;
				pageData(data);
			}
		}
	})
};
var w;
var myscroll;
//是否正在加载中	true表示正在加载  false表示没有加载
var is_r = false;
function pageData(sessionid) {
	$.ajax({
		type: 'POST',
		url: baseUrl + 'v3.0/action/list',
		contentType: 'application/json',
		data: JSON.stringify(page),
		headers: {'session_id': sessionid},
		success: function (result) {
			if (result.data_list.length!=0){
                var timestamp = result.data_list[0].create_time;
			}else{
                var timestamp;
			}


			//注册vue
			w = new Vue({
				el: "#wrapper",
				data: {
					static: "",
					datainfos: result.data_list
				},
				methods: {
					my: function () {
						window.location.href = "销售雷达/我的.html?sessionid=" + sessionid;
					},
					custom: function () {
						window.location.href = "销售雷达/客户.html?sessionid=" + sessionid;
					},
					messge: function () {

						window.location.href = "销售雷达/消息.html?sessionid=" + sessionid;
					},
					show: function (create_time) {
						if (timestamp == create_time) {
							return true;
						}
						else if (timestamp - create_time > 5 * 60 * 1000) {
							timestamp = create_time;
							return true;
						} else {
							return false;
						}
					}
				}
			})

			myscroll = new iScroll("wrapper",{
				topOffset: 0,
				//上拉时触发
				onScrollMove: function(){
					w.static = 0;
					//如果上拉高度 大于 (内容高度 - wrapper高度) 50px 以上  且是未刷新状态时触发 ;
					//if(is_r == false){
                    if(this.y <= ( this.wrapperH - this.scroller.clientHeight ) && is_r == false){
						//正在加载状态
						is_r = true;
						setTimeout(function(){
							//这里表示数据加载成功后
							/*for (var i = 0;i<3;i++){
								w.datainfos.push({name:"+"});
							}*/
                            nextPage(w.datainfos);
							//这里表示渲染完成后刷新wrapper
							setTimeout(function(){
								console.log("刷新wrapper");
								//显示加载成功状态图标 (没有更多数据时候的提示作用)
								w.static = 2;
								setTimeout(function(){
									w.static = "";
								},500)
								//加载完成状态
								is_r = false;
								myscroll.refresh();
							},0)
						},2000)

					}
				},
				onScrollEnd: function(){
					//上拉之后如果触发刷新则 状态图标值为1 显示loading状态
					if(is_r == true){
						w.static = 1;
					}
				}
			});
		}

	})
}
function nextPage(that){
	++page.page_num;
    $.ajax({
        type: 'POST',
        url: baseUrl + 'v3.0/action/list',
        contentType: 'application/json',
        data: JSON.stringify(page),
        headers: {'session_id': sessionid},
        success: function (data) {
        	for(var i in data.data_list)
			{
                that.push(data.data_list[i]);
			}

        }
    })
}
function custom () {
    window.location.href = "销售雷达/客户.html?sessionid=" + sessionid;
}
function my () {
    window.location.href = "销售雷达/我的.html?sessionid=" + sessionid;
}
function messge () {

    window.location.href = "销售雷达/消息.html?sessionid=" + sessionid;
}