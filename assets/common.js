//服务器地址
//var baseUrl='https://keji-api.h5h5h5.cn/push/';
//var baseUrl='http://120.77.82.251:9001/push/';
var baseUrl='https://keji-api.h5h5h5.cn/push/';

/**
 * 获取url中的参数
 * @param name
 * @returns {*}
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null;
}
/**
 * 时间戳转化成时间
 * @param timestamp
 * @returns {*}
 */
function timestampToTime (timestamp){
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = date.getDate() < 10 ? '0'+date.getDate():date.getDate();
    h = (date.getHours() < 10 ? '0'+date.getHours():date.getHours()) + ':';
    m = (date.getMinutes() < 10 ? '0'+date.getMinutes():date.getMinutes())  + ':';
    s = date.getSeconds() < 10 ? '0'+date.getSeconds():date.getSeconds();
    return Y+M+D+' '+h+m+s;
}
function randomString(len) {
    len = len || 32;
    var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = $chars.length;
    var pwd = '';
    for (i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}