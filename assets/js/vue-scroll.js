var code = getQueryString("code");
var sessionid = getQueryString("sessionid");
var page = {
    "page_num": 1,
    "page_size": 8
}
var pagenum=1;
$(function () {
    if (sessionid == null) {
        aiUserInfo();
    } else {
        pageData(sessionid)
    }

})


/**
 * 获取用户信息及权限
 */
function aiUserInfo() {
    $.ajax({
        type: 'POST',
        url: baseUrl + 'v3.0/login/ai?code=' + code,
        contentType: 'application/json',
        success: function (data) {
            //无权限 则跳转
            if (data == 0) {
                window.location.href = "企业微信/没有权限.html";
            }
            //有权限
            else {

                pageData(data);
            }
        }
    })
};
var w;
function pageData(sessionid) {
    $.ajax({
        type: 'POST',
        url: baseUrl + 'v3.0/action/list',
        contentType: 'application/json',
        data: JSON.stringify(page),
        headers: {'session_id': sessionid},
        success: function (result) {
            var timestamp = result.data_list[0].create_time;

            //注册vue
            w = new Vue({
                el: "#wrapper",
                data: {
                    static: "",
                    datainfos: result.data_list
                },
                methods: {
                    my: function () {
                        window.location.href = "销售雷达/我的.html?sessionid=" + sessionid;
                    },
                    custom: function () {
                        window.location.href = "销售雷达/客户.html?sessionid=" + sessionid;
                    },
                    messge: function () {

                        window.location.href = "销售雷达/消息.html?sessionid=" + sessionid;
                    },
                    show: function (create_time) {
                        if (timestamp == create_time) {
                            return true;
                        }
                        else if (timestamp - create_time > 5 * 60 * 1000) {
                            timestamp = create_time;
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            })


        }

    })
}

